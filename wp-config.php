<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'trongdong');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '~KsLkg/loX?iWc;aD2:c<cU|a%(zgXu&:Zb}`L)d@9ox::Wq]#DonO08J>5mloEz');
define('SECURE_AUTH_KEY',  'rP)nJLC<?jj28p[Izbjhux}dRXrozi%rt.GwI<cN+FXxEL_UhWMTG(38p^BQ+g8*');
define('LOGGED_IN_KEY',    '$B]+6+n {/5{iHpG@xC/ukU2^NhoCRbfv!n*Jy!$l[OojufZqTY3Oq_HDEYC;f;2');
define('NONCE_KEY',        '[+rS^Bzw9L^ 2av?a<{<Joax|QG?bn{WzA!H6o[Cwsu+XO,w:<Ehjx@t1^0;16nu');
define('AUTH_SALT',        '1eQ[W-b,A<Zc[x9d=/<D;Y7MeT9P[l@HT]s?&>+%Uz$rg-}Jz7n>V9]|pF-oC/%L');
define('SECURE_AUTH_SALT', 'nuO<C0kacelz$_uPt#8vw}O%L>%c<fk|^2{wK4Sk@x_xD{c%)+&@w,Iagwu{ln&7');
define('LOGGED_IN_SALT',   'cA`S])Uig%bnU3$dSXunnmkVu]t4 aM_;N:f]w=/AsN-GZ97 !=L8*R1V[RA B-Y');
define('NONCE_SALT',       'xI^`}O=1uxWEB&I0Ng4X3rZR~uWR5Ah}h&g2n0[/ThQp,.>}zu1fsb6>{JwBY7IJ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'td_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
// define('WP_DEBUG', false);
define('FS_METHOD', direct);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
