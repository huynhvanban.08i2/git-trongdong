<?php 
/**
 * Template Name: Contact Page Template
 *
 */
get_header(); ?>

    <section class="page-head">
        <article class="container">
            <h3><?php the_title(); ?></h3>
            <div class="services-breadcrumb">
                <?php custom_breadcrumbs(); ?>
            </div>
        </article>
    </section>
    <section class="map">
        <?php if(!empty($tp_options['mapContact'])) {?>
            <?php echo $tp_options['mapContact']; ?>
        <?php }; ?>
    </section>
    <section class="content-page content-page-contact">
        <article class="container">
            <div class="row">
                <div class="col-md-6 address-grid">
                    <h4>For More <span>Information</span></h4>
                    <?php if(!empty($tp_options['telephone'])) {?>
                        <div class="mail-agileits-w3layouts">
                            <i class="fa fa-mobile" aria-hidden="true"></i>
                            <div class="contact-right">
                                <p>Telephone </p><span><?php echo $tp_options['telephone']; ?></span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    <?php }; ?>
                    <?php if(!empty($tp_options['email'])) {?>
                        <div class="mail-agileits-w3layouts">
                            <i class="fa fa-envelope-o" aria-hidden="true"></i>
                            <div class="contact-right">
                                <p>Mail </p><a href="mailto:<?php echo $tp_options['email']; ?>"><?php echo $tp_options['email']; ?></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    <?php }; ?>
                    <?php if(!empty($tp_options['localtion'])) {?>
                        <div class="mail-agileits-w3layouts">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                            <div class="contact-right">
                                <p>Location</p><span><?php echo $tp_options['localtion']; ?></span>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    <?php }; ?>
                </div>

                <div class="col-md-6 contact-form">
                    <h4 class="white-w3ls">Contact <span>Form</span></h4>
                    <?php echo do_shortcode('[contact-form-7 id="39" title="Contact Page"]'); ?>
                </div>
            </div>
        </article>
    </section>
    <!-- End /content -->

<?php get_footer(); ?>