<?php 
/**
 * Template Name: Blog Page Template
 *
 */
get_header(); ?>

<div class="container">
    <?php custom_breadcrumbs(); ?>
    <h1 class="page-title"<?php the_title(); ?></h1>
</div>


<div class="container">
    <div class="row blog-page">
        <div class="col-md-3">
            <?php get_sidebar(); ?>
        </div>
        <div class="col-md-9">
            <!-- START BLOG POST -->
            <?php
                $args = array(

                        'post_type'         => 'post',
                        'paged'             => get_query_var( 'paged' ),
                        'order'             => 'DESC',

                    );

                $wp_query = new WP_Query($args);
            ?>

            <?php if ( $wp_query->have_posts() ) while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?> 
            <div class="article post row">
                <div class="post-header col-md-5">
                    <?php if(has_post_thumbnail( $post_id )) {?>
                        <a class="hover-img" href="<?php the_permalink(); ?>">
                                <?php the_post_thumbnail();?>
                        </a>
                    <?php } ?>
                </div>
                <div class="post-inner col-md-7">
                    <h4 class="post-title"><a class="text-darken" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                    <ul class="post-meta">
                        <li><i class="fa fa-calendar"></i><a><?php the_date();?></a>
                        </li>
                        <li><i class="fa fa-user"></i><a><?php the_author();?></a>
                        </li>
                        <li><i class="fa fa-tags"></i><a><?php the_category(' ');?></a>
                        </li>
                    </ul>
                    <p class="post-desciption"><?php the_content_rss(' ',false, '', 40) ?></p>
                    <a class="btn btn-small btn-primary" href="<?php the_permalink(); ?>">Read More</a>
                </div>
            </div>
            <?php endwhile; // end of the loop. ?> 
            
            <!-- <ul class="pagination">
                <li class="active"><a href="#">1</a>
                </li>
                <li><a href="#">2</a>
                </li>
                <li><a href="#">3</a>
                </li>
                <li><a href="#">4</a>
                </li>
                <li><a href="#">5</a>
                </li>
                <li><a href="#">6</a>
                </li>
                <li><a href="#">7</a>
                </li>
                <li class="dots">...</li>
                <li><a href="#">43</a>
                </li>
                <li class="next"><a href="#">Next Page</a>
                </li>
            </ul> -->
        </div>
    </div>
</div>



<div class="gap"></div>


<?php get_footer();
