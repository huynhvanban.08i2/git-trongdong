<?php get_header(); ?>
    <section class="page-head">
        <article class="container">
            <h3><?php the_title(); ?></h3>
            <div class="services-breadcrumb">
                <?php custom_breadcrumbs(); ?>
            </div>
        </article>
    </section>
    <section class="content-page">
        <article class="container">
            <div class="row">
                <?php while ( have_posts() ) : the_post(); ?>
                    <?php the_content(); ?>
                <?php endwhile; ?>
                <div class="clearfix"></div>
            </div>
        </article>
    </section>
    <!-- End /content -->

<?php get_footer(); ?>