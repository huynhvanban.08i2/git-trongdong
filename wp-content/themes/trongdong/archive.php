<?php
/**
 * The template for displaying archive pages
 */

get_header(); ?>

<section class="page-head">
    <article class="container">
    	<h3><?php echo single_cat_title( '', false );  ?></h3>
        <div class="services-breadcrumb">
            <?php custom_breadcrumbs(); ?>
        </div>
    </article>
</section>
<!-- End section Breadcrumbs -->

<section class="content-page">
    <article class="container">
        <div class="post-content">
            <div class="row">
                <!-- START BLOG POST -->
                <?php
				if ( have_posts() ) : ?>
					<?php
					/* Start the Loop */
					while ( have_posts() ) : the_post(); ?>
                    <div class="col-sm-3 col-xs-6 list-post">
                        <div class="thumb">
                            <div class="main-thumb">
                                <?php if(has_post_thumbnail()) { ?>
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                        <?php the_post_thumbnail('home-post-thumb'); ?>
                                    </a>
                                <?php  }; ?>
                            </div>
                        </div>
                        <h3><?php the_title(); ?></h3>
                        <p><?php the_content_rss(' ',false, '', 70) ?></p>
                        <a class="more" href="<?php the_permalink(); ?>" title="Đọc tiếp">Đọc tiếp</a>
                    </div>
	                <?php endwhile;

					else :


					endif; 
				?>
            </div>
        </div>
    </article>
</section>
<!-- End /content -->
<?php get_footer(); ?>