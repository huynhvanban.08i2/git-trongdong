        <footer class="footer">
            <div class="container">
                <?php dynamic_sidebar('footer-1') ?>
                <div class="col-sm-3 col-footer">
                    <h2>FOLLOW US</h2>
                    <div class="post-footer footer-rss">
                        <?php global $tp_options; ?>
                        <?php if(!empty($tp_options['socialrss'])) {?>
                            <a href="<?php echo $tp_options['socialrss']; ?>" title="rss"><img src="<?php bloginfo('template_url')?>/images/rss.png" alt="rss"></a>
                        <?php } ?>
                        <?php if(!empty($tp_options['socialtwitter'])) {?>
                            <a href="<?php echo $tp_options['socialtwitter']; ?>" title="twitter"><img src="<?php bloginfo('template_url')?>/images/twitter.png" alt="twitter"></a>
                        <?php } ?>
                        <?php if(!empty($tp_options['socialfacebook'])) {?>
                            <a href="<?php echo $tp_options['socialfacebook']; ?>" title="facebook"><img src="<?php bloginfo('template_url')?>/images/facebook.png" alt="facebook"></a>
                        <?php } ?>
                        <?php if(!empty($tp_options['socialflickr'])) {?>
                            <a href="<?php echo $tp_options['socialflickr']; ?>" title="flickr"><img src="<?php bloginfo('template_url')?>/images/flickr.png" alt="flickr"></a>
                        <?php } ?>
                        <?php if(!empty($tp_options['socialyoutube'])) {?>
                            <a href="<?php echo $tp_options['socialyoutube']; ?>" title="youtube"><img src="<?php bloginfo('template_url')?>/images/youtube.png" alt="youtube"></a>
                        <?php } ?>
                        <?php if(!empty($tp_options['sociallinkedin'])) {?>
                            <a href="<?php echo $tp_options['sociallinkedin']; ?>" title="linkedin"><img src="<?php bloginfo('template_url')?>/images/linkedin.png" alt="linkedin"></a>
                        <?php } ?>
                        <?php if(!empty($tp_options['socialfoursquare'])) {?>
                            <a href="<?php echo $tp_options['socialfoursquare']; ?>" title="foursquare"><img src="<?php bloginfo('template_url')?>/images/foursquare.png" alt="foursquare"></a>
                        <?php } ?>
                        <?php if(!empty($tp_options['socialdelicious'])) {?>
                            <a href="<?php echo $tp_options['socialdelicious']; ?>" title="delicious"><img src="<?php bloginfo('template_url')?>/images/delicious.png" alt="delicious"></a>
                        <?php } ?>
                        <?php if(!empty($tp_options['socialdigg'])) {?>
                            <a href="<?php echo $tp_options['socialdigg']; ?>" title="digg"><img src="<?php bloginfo('template_url')?>/images/digg.png" alt="digg"></a>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-sm-3 col-footer">
                    <h2>JOIN OUR CREW</h2>
                    <div class="post-footer">
                        <form action="" method="get">
                            <label>Email Address: </label>
                            <input type="email" placeholder="">
                            <input type="submit" name="Subscribe" value="Subscribe">
                        </form>
                    </div>
                </div>
            </div>
        </footer>
        <!-- End /footer -->
        <div class="loading-trongdong">
            <div class="left-trongdong"></div>
            <div class="right-trongdong"></div>
        </div>
        <?php wp_footer(); ?>
    </body>
</html>