<?php
/**
 * The template for displaying 404 pages (not found)
 *
 */

get_header(); ?>
<section id="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?php custom_breadcrumbs(); ?>
            </div>
        </div>
    </div>
</section>
<!-- End section Breadcrumbs -->

<section class="content-page locations-page">
    <article class="container">
        <h1>Page is not found</h1>
        <br>
        <br>
        <a class="btn btn-white btn-ghost btn-lg mt5" href="<?php echo home_url( '/' ); ?>"><i class="fa fa-long-arrow-left"></i> to Homepage</a>
    </article>
</section>

<?php get_footer();
