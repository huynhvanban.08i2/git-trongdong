<?php 
/**
 * Template Name: Contact Page Template
 *
 */
get_header(); ?>

    <section id="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <?php custom_breadcrumbs(); ?>
                </div>
            </div>
        </div>
    </section>
    <!-- End section Breadcrumbs -->

    <section class="block-title-page-menu contact-title" style="background-image: url(<?php the_post_thumbnail_url(); ?>); ">
        <div class="bg-mask"></div>
        <article class="container">
            <h1><?php the_title();?></h1>
        </article>
    </section>
    <!-- End section Title Images Menu -->

    <section class="content-page contact-page">
        <article class="container">
            <?php
            // Start the loop.
            while ( have_posts() ) : the_post(); ?>
            <div class="row">
                <div class="col-md-7">
                    <?php the_content(); ?>
                    <?php echo do_shortcode('[contact-form-7 id="136" title="Contact form"]');?>
                </div>
                <div class="col-md-4">
                    <aside class="sidebar-right">
                        <?php echo $tp_options['address']; ?>
                    </aside>
                </div>
            </div>
            <?php // End the loop.
            endwhile;
        ?>
        </article>
    </section>
    <!-- End section Location Page-->
    

<?php get_footer();
