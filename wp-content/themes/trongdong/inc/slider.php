<div class="slider">
    <div class="main-slider">
        <?php
            $args = array(
                    'post_type'         => 'slider',
                    'paged'             => get_query_var( 'paged' ),
                    'order'             => 'DESC',
                );

            $wp_query = new WP_Query($args);
        ?>
        <?php if ( $wp_query->have_posts() ) while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?> 
            <?php if(has_post_thumbnail()) {?>
                <div class="list-slider row">
                    <div class="col-sm-6 text-slider hidden-xs">
                        <?php the_content(); ?>
                    </div>
                    <div class="col-sm-6 thumb-slider">
                        <div class="shadow-thumb">
                            <a href="<?php the_permalink(); ?>" title="title">
                                <?php the_post_thumbnail('thumb-slider') ?>
                            </a>
                        </div>
                    </div>
                </div>
            <?php }; ?>
        <?php endwhile; ?> 
    </div>
</div>