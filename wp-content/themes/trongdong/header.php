<?php
/**
 * The header for our theme
 *
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head();?>
<?php global $tp_options; ?>
</head>

<body <?php body_class(); ?>>
    <header class="header">
        <div class="top-head text-right">
            <div class="container">
                <?php if(!empty($tp_options['linkOne'])) {?>
                    <a target="_blank" href="<?php echo $tp_options['linkOne']; ?>" title="link site"><?php echo $tp_options['linkOne']; ?></a>
                <?php }; ?>
                <?php if(!empty($tp_options['linkTwo'])) {?>
                    <a target="_blank" href="<?php echo $tp_options['linkTwo']; ?>" title="link site"><?php echo $tp_options['linkTwo']; ?></a>
                <?php }; ?>
            </div>
        </div>
    </header>
    <section class="banner <?php if(!is_home() || !is_front_page()){ echo 'banner-page'; }?>">
        <article class="container">
            <nav class="navbar">
                <div class="row">
                    <div class="navbar-mobile-menu col-xs-12 visible-xs-inline-block hidden-sm">
                        <div class="button-menu">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                    <?php wp_nav_menu( 
                        array( 
                            'theme_location' => 'top', 
                            'menu_class' => 'nav navbar-nav hidden-xs'
                        ) 
                    ); ?>
                </div>
            </nav>
            <?php if(is_home() || is_front_page()){?>
                <?php get_template_part( 'inc/slider', 'slider' ); ?>
            <?php }; ?>
        </article>
    </section>
    <!-- /slider --> 