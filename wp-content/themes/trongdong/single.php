<?php get_header(); ?>
    <section class="page-head">
        <article class="container">
            <h3><?php the_title(); ?></h3>
            <div class="services-breadcrumb">
                <?php custom_breadcrumbs(); ?>
            </div>
        </article>
    </section>
    <section class="content-page single-page">
        <article class="container">
            <?php while ( have_posts() ) : the_post(); ?>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-7 left-signle-location">
                    <h1 class="title-block"><?php the_title(); ?></h1>
                    <div class="main-block">
                        <?php the_content(); ?>
                    </div>
                </div>
                <?php if(has_post_thumbnail()) {?>
                <div class="col-xs-12 col-sm-6 col-md-5 thumbnail">
                    <?php the_post_thumbnail();?>
                </div>
                <?php } ?>
            </div>
            <?php endwhile; // End of the loop. ?>          
        </article>
    </section>
    <!-- End section Location Page-->

<?php get_footer(); ?>