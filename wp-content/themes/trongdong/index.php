<?php get_header(); ?>
    <section class="content">
        <article class="container">
            <?php if(!empty($tp_options['titleHome'])) {?>
                <h3 class="title-home text-center"><?php echo $tp_options['titleHome']; ?></h3>
            <?php }; ?>
            <div class="post-content">
                <div class="row">
                    <!-- START BLOG POST -->
                    <?php
                        $args = array(

                                'post_type'         => 'post',
                                'paged'             => get_query_var( 'paged' ),
                                'order'             => 'DESC',
                                'posts_per_page'   => 4,
                            );

                        $wp_query = new WP_Query($args);
                    ?>

                    <?php if ( $wp_query->have_posts() ) while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?> 
                        <div class="col-sm-3 col-xs-6 list-post">
                            <div class="thumb">
                                <div class="main-thumb">
                                    <?php if(has_post_thumbnail()) { ?>
                                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                            <?php the_post_thumbnail('home-post-thumb'); ?>
                                        </a>
                                    <?php  }; ?>
                                </div>
                            </div>
                            <h3><?php the_title(); ?></h3>
                            <p><?php the_content_rss(' ',false, '', 70) ?></p>
                            <a class="more" href="<?php the_permalink(); ?>" title="Đọc tiếp">Đọc tiếp</a>
                        </div>
                    <?php endwhile; // end of the loop. ?> 
                </div>
            </div>
        </article>
    </section>
    <!-- End /content -->
<?php get_footer(); ?>