<?php
    if ( ! class_exists( 'mrban_Theme_Options' ) ) {

    /* class mrban sẽ chứa toàn bộ code tạo options trong theme từ Redux Framework */
  	class mrban_Theme_Options {
        /* Tái tạo các biến có trong Redux Framework */
		public $args = array();
		public $sections = array();
		public $theme;
		public $ReduxFramework;

		/* Load Redux Framework */
		public function __construct() {
		 
		    if ( ! class_exists( 'ReduxFramework' ) ) {
		        return;
		    }
		 
		    // This is needed. Bah WordPress bugs.  <img draggable="false" class="emoji" alt="😉" src="https://s.w.org/images/core/emoji/2.2.1/svg/1f609.svg">
		    if ( true == Redux_Helpers::isTheme( __FILE__ ) ) {
		         $this->initSettings();
		    } else {
		         add_action( 'plugins_loaded', array( $this, 'initSettings' ), 10 );
		    }
		 
		}

		/**Thiết lập các method muốn sử dụng ,Method nào được khai báo trong này thì cũng phải được sử dụng **/
		public function initSettings() {
 
		    // Set the default arguments
		    $this->setArguments();
		 
		    // Set a few help tabs so you can see how it's done
		    $this->setHelpTabs();
		 
		    // Create the sections and fields
		    $this->setSections();
		 
		    if ( ! isset( $this->args['opt_name'] ) ) { // No errors please
		        return;
		    }
		 
		    $this->ReduxFramework = new ReduxFramework( $this->sections, $this->args );
		}

		/**
		Thiết lập cho method setAgruments
		Method này sẽ chứa các thiết lập cơ bản cho trang Options Framework như tên menu chẳng hạn
		**/
		public function setArguments() {
		    $theme = wp_get_theme(); // Lưu các đối tượng trả về bởi hàm wp_get_theme() vào biến $theme để làm một số việc tùy thích.
		    $this->args = array(
	            // Các thiết lập cho trang Options
	            'opt_name'  => 'tp_options', // Tên biến trả dữ liệu của từng options, ví dụ: tp_options['field_1']
	            'display_name' => $theme->get( 'Name' ), // Thiết lập tên theme hiển thị trong Theme Options
	            'menu_type'          => 'menu',
		        'allow_sub_menu'     => true,
		        'menu_title'         => __( 'Theme Options', 'mrban' ),
		        'page_title'         => __( 'Theme Options', 'mrban' ),
		        'dev_mode' => false,
		        'customizer' => true,
		        'menu_icon' => '', // Đường dẫn icon của menu option
		        // Chức năng Hint tạo dấu chấm hỏi ở mỗi option để hướng dẫn người dùng */
		        'hints'              => array(
		            'icon'          => 'icon-question-sign',
		            'icon_position' => 'right',
		            'icon_color'    => 'lightgray',
		            'icon_size'     => 'normal',
		            'tip_style'     => array(
		                'color'   => 'light',
		                'shadow'  => true,
		                'rounded' => false,
		                'style'   => '',
		            ),
		            'tip_position'  => array(
		                'my' => 'top left',
		                'at' => 'bottom right',
		            ),
		            'tip_effect'    => array(
		                'show' => array(
		                    'effect'   => 'slide',
		                    'duration' => '500',
		                    'event'    => 'mouseover',
		                ),
		                'hide' => array(
		                    'effect'   => 'slide',
		                    'duration' => '500',
		                    'event'    => 'click mouseleave',
		                ),
		            ),
		        ) // end Hints
		    );	    
  		}

  		/**
		Thiết lập khu vực Help để hướng dẫn người dùng
		**/
		public function setHelpTabs() {
		 
		    // Custom page help tabs, displayed using the help API. Tabs are shown in order of definition.
		    $this->args['help_tabs'][] = array(
		        'id'      => 'redux-help-tab-1',
		        'title'   => __( 'Theme Information 1', 'thachpham' ),
		        'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'thachpham' )
		    );
		 
		    $this->args['help_tabs'][] = array(
		        'id'      => 'redux-help-tab-2',
		        'title'   => __( 'Theme Information 2', 'thachpham' ),
		        'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'thachpham' )
		    );
		 
		    // Set the help sidebar
		    $this->args['help_sidebar'] = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'thachpham' );
		}

		/**
		Thiết lập từng phần trong khu vực Theme Options
		mỗi section được xem như là một phân vùng các tùy chọn
		Trong mỗi section có thể sẽ chứa nhiều field
		**/
		public function setSections() {
		 
		    // header Section
		    $this->sections[] = array(
		        'title'  => __( 'Header', 'mrban' ),
		        'desc'   => __( 'All of settings for top header on this theme.', 'mrban' ),
		        'icon'   => 'el-icon-link',
		        'fields' => array(
				    array(
				        'id'       => 'linkOne',
				        'type'     => 'text',
				        'title'    => __( 'Link header 1', 'mrban' ),
				    ),
				    array(
				        'id'       => 'linkTwo',
				        'type'     => 'text',
				        'title'    => __( 'Link header 2', 'mrban' ),
				    ),
		        )
		    ); // end section

		    // Slider Section
		    $this->sections[] = array(
		        'title'  => __( 'Home Settings', 'mrban' ),
		        'desc'   => __( 'All of settings for Home on this theme.', 'mrban' ),
		        'icon'   => 'el-icon-home',
		        'fields' => array(
		        	array(
				        'id'       => 'titleHome',
				        'type'     => 'text',
				        'title'    => __( 'Title', 'mrban' ),
				    ),
		        )
		    ); // end section

		    // Slider Section
		    $this->sections[] = array(
		        'title'  => __( 'Social Settings', 'mrban' ),
		        'desc'   => __( 'All of settings for social on this theme.', 'mrban' ),
		        'icon'   => 'el-icon-facebook',
		        'fields' => array(
		        	array(
				        'id'       => 'socialrss',
				        'type'     => 'text',
				        'title'    => __( 'Rss', 'mrban' ),
				    ),
				    array(
				        'id'       => 'socialtwitter',
				        'type'     => 'text',
				        'title'    => __( 'Twitter', 'mrban' ),
				    ),
				    array(
				        'id'       => 'socialfacebook',
				        'type'     => 'text',
				        'title'    => __( 'Facebook', 'mrban' ),
				    ),
				    array(
				        'id'       => 'socialflickr',
				        'type'     => 'text',
				        'title'    => __( 'Flickr', 'mrban' ),
				    ),
				    array(
				        'id'       => 'socialyoutube',
				        'type'     => 'text',
				        'title'    => __( 'Youtube', 'mrban' ),
				    ),
				    array(
				        'id'       => 'sociallinkedin',
				        'type'     => 'text',
				        'title'    => __( 'Linkedin', 'mrban' ),
				    ),
				    array(
				        'id'       => 'socialfoursquare',
				        'type'     => 'text',
				        'title'    => __( 'Foursquare', 'mrban' ),
				    ),
				    array(
				        'id'       => 'socialdelicious',
				        'type'     => 'text',
				        'title'    => __( 'Delicious', 'mrban' ),
				    ),
				    array(
				        'id'       => 'socialdigg',
				        'type'     => 'text',
				        'title'    => __( 'Digg', 'mrban' ),
				    ),
		        )
		    ); // end section

		    // Brand Section
		    $this->sections[] = array(
		        'title'  => __( 'Contact Address', 'mrban' ),
		        'desc'   => __( 'All of settings for address on this theme.', 'mrban' ),
		        'icon'   => 'el-icon-phone',
		        'fields' => array(
				    array(
				        'id'       => 'telephone',
				        'type'     => 'text',
				        'title'    => __( 'Telephone', 'mrban' ),
				    ),
				    array(
				        'id'       => 'email',
				        'type'     => 'text',
				        'title'    => __( 'Mail', 'mrban' ),
				    ),
				    array(
				        'id'       => 'localtion',
				        'type'     => 'text',
				        'title'    => __( 'Localtion', 'mrban' ),
				    ),
				    array(
				        'id'       => 'mapContact',
				        'type'     => 'editor',
				        'title'    => __( 'Map', 'mrban' ),
				    ),
		        )
		    ); // end section

		}
	}

  	/* Kích hoạt class mrban_Theme_Options vào Redux Framework */
    global $reduxConfig;
    $reduxConfig = new mrban_Theme_Options();
  }
?>