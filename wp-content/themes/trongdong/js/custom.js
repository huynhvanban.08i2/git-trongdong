jQuery(function($) {
	$(document).ready(function(){
		$('.slider .main-slider').bxSlider({
			mode: 'fade',
			auto: true,
		});
	});

	//menu mobile
    $(function() {
    	// add class active 
	    $('.button-menu').click(function() {	
	    	$(this).toggleClass('active');	    	
	    	$('.navbar-nav').toggleClass('navbar-nav-mobile');
	    });

	    function openTrongDong(){
			$('.loading-trongdong div').addClass('active');
		}
		setTimeout(openTrongDong, 3000);

		function hideTrongDong(){
			$('.loading-trongdong').hide('slow');
		}
		setTimeout(hideTrongDong, 4000);

    });	 

    // form contact
    $(function() {
    	// add class focus 
	    $('.contact-form input').focus(function() {	
	    	$(this).addClass('focus');
	    });
	    $('.contact-form input').focusout(function() {	
	    	if ($(this).val() == '') {
	    		$(this).removeClass('focus');
	    	}
	    });
	    $('.contact-form textarea').focus(function() {	
	    	$(this).addClass('focus');
	    });
	    $('.contact-form textarea').focusout(function() {	
	    	if ($(this).val() == '') {
	    		$(this).removeClass('focus');
	    	}
	    });
    });	

    // add class thumb product
    $(function() {
    	$('.single-pro .men-thumb-item img').addClass('pro-image-front');
    }); 


});
